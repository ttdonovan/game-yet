# Notes

Oh it's you! What are you doing here? Is this part of the game or just a collection of development notes? Maybe we should turn back and do something else, I wary of sleeping dragons.

## Ideas

* mdBook for the **game-manual**
* Godot for pilot first game
* Git for time travel but questions on how to implement it into the narrative
* along with a `CHANGELOG` and screenshot images of game progress (could they also be included in **game-manual**)

## Game Design

* game story is driven by **game-manual**
* game play can be any "game" or "mini-game" in **game-yet**, starts with `ep0_pilot`
* use `git` to work in a time traveling compoment, done through **game-manual** and `git-commits`(?)
* teach game-dev from with in the game, but how?

## TODOs

* [x] create foundation for a "game manual" and guide for next steps
* [x] add a **dependencies** section to `README.md`
* [ ] add `/support` folder that includes docs on how to setup environments - Mac, Windows, Linux(?)
* [ ] add a **contents** section to `README.md` to explain folder structure
* [ ] add a `CHANGELOG.md` to journal the changes made to **game-yet**
* [ ] research if there is something like gh-pages for gitlab to host **game-manual**
* [ ] call to action for art, design, development, etc.
* [ ] research how to properly attribute art add a `CREDITS` file?

## DOINGs

*  create the start of the game - a pilot before episode one
    * is this a 2D top-down or platformer?
