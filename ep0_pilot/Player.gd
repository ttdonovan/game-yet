extends KinematicBody2D

const MAX_SPEED = 280
const MOVE_MULTI = 600

var accel = Vector2(0, 0)
var velocity = Vector2(0, 0)
var direction = Vector2(0, 0)

var sprite
var frameCount = 0
var deltaTime = 0

func _animateSprite(frame):
	if deltaTime > 0.1:
		sprite.set("frame", frame + frameCount)

		if frameCount < 7:
			frameCount += 1
		else:
			frameCount = 0

		deltaTime = 0
	pass

func _ready():
	sprite = get_node("Sprite")
	pass

func _process(delta):
	deltaTime = deltaTime + delta
	pass

func _physics_process(delta):
	if Input.is_action_pressed("move_left"):
		direction.x = -1
		_animateSprite(117);
	elif Input.is_action_pressed("move_right"):
		direction.x = 1
		_animateSprite(143);
	else:
		direction.x = 0

	if Input.is_action_pressed("move_up"):
		direction.y = -1
		_animateSprite(104);
	elif Input.is_action_pressed("move_down"):
		direction.y = 1
		_animateSprite(130);
	else:
		direction.y = 0

	if direction.x != 0:
		accel.x += MOVE_MULTI * delta
	else:
		accel.x -= MOVE_MULTI * delta

	if direction.y != 0:
		accel.y += MOVE_MULTI * delta
	else:
		accel.y -= MOVE_MULTI * delta

	accel.x = clamp(accel.x, 0, MAX_SPEED)
	accel.y = clamp(accel.y, 0, MAX_SPEED)

	velocity = accel * direction
	move_and_slide(velocity, Vector2(0, -1))

	if velocity == Vector2(0, 0):
		sprite.set("frame", 130)

	pass
