# [DRAFT] "Untitled" - Game Yet?

You might not have realize it but just by reading this you've already started
playing the game. You are the protagonist in this story, whom is also the
players and game makers. This is the inception, think of yourself as a
multiverse [^1], time traveling [^2], quantum leaping [^3] demigod [^4]. Sure
you could quit and stop playing right now but then what is it that you have
achieved... nothing. If this "Untitled" game has a score board you would
receive a big fat "Zero" (but then again may this game may never keep score, I
guess that is _yet_ to be determined).

"Who am I?" you may be asking yourself. I am the narrator, the inner voice of
all the game makers (and yes I know that does include you). I will be your
dungeon master [^5] as we take this journey together in "Untitled" - Game Yet?

Still playing? Good. Where to begin this journey... maybe start with a [Game
Manual](#game-manual)? But before we can browse the game manual a few
[Dependencies](#dependencies) must first be acquired.

## Dependencies

* [Rust](https://www.rust-lang.org/en-US/) via [rustup](https://rustup.rs/)
* [mdBook](https://github.com/rust-lang-nursery/mdBook)
* [Godot](https://godotengine.org/)

## Game Manual

Usage:

```
mdbook serve game-manual
open http://localhost:3000
```

## Ep0: The Pilot

The `ep0_pilot` is a Godot project. Download and install Godot then open the
project and run.

---

[^1]: [Marvel Universe](https://en.wikipedia.org/wiki/Marvel_Universe)
[^2]: [The Terminator](https://en.wikipedia.org/wiki/The_Terminator)
[^3]: [Quantum Leap](https://en.wikipedia.org/wiki/Quantum_Leap)
[^4]: [Demigod](https://en.wikipedia.org/wiki/Demigod_(video_game))
[^5]: [Dungeon Master](https://en.wikipedia.org/wiki/Dungeon_Master)

## Legal

### License and Attribution

* [LICENSE](./LICENSE)
* [OpenGameArt LPC Collection](https://opengameart.org/content/lpc-collection)
    * [Male/Orc](https://opengameart.org/content/lpc-male-sheets)
    * [Skeleton](https://opengameart.org/content/lpc-skeleton)
