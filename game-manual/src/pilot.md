# Pilot: The Inception

At first there was nothing... hopefully one of these game makers implements
something to get us started.

Behold there is a [Godot](https://godotengine.org/) project called `ep0_pilot`.
Open the project and run...

> Use `W`, `A`, `S`, `D` to move around.

* See the `World.tscn` which contains a `World2D` with `RichTextLabel` and a
`World2D.gd` GDScript.
* See the `Player.tscn` which contains a `KinematicBody2D` with
`CollisionShape2D` and `Sprite`. The `Player.gd` script controls the movement
of the `KinematicBody2D` in the world.
* See `InvisibleWalls2D` in the `Wolrd.tscn` which are `StaticBody2D` and
`CollisionShape2D`.

You are a free sprited warrior that has no boundaries, execpt for those pesky
invisible walls. No seriously be carful of the edges else you may fall off the
edge of the World.
