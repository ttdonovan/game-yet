# Resources

Looking for tips and tricks? Here are a handful of useful pointers and
suggestions.

## Tips & Pointers

* Documentation is written in [Markdown](https://en.wikipedia.org/wiki/Markdown)
* Manage source control with [Git](https://git-scm.com/)
* Godot/GDScript have a syntax very similar to [Python](https://www.python.org/)
* When running Godot Scenes - you can play a select scene or custom scene
    * Play Scene (Command + R)
    * Quick Run Scene (Command + Shift + R)
    * A "Main Scene" can be set under Project -> Project Settings -> Run -> Main Scene (`res://World.tscn`)
    * A `Wolrd.tscn` is usually want you want to run to play the entire game

## Suggested Applications

* A Mac OS package manager: [Homebrew](https://brew.sh/)
* A Markdown application: [MacDown](https://macdown.uranusjr.com/)
* Two options for code editiors: [Atom](https://atom.io/) or [Visual Studio Code](https://code.visualstudio.com/)

## Additional Documentation

* https://rust-lang-nursery.github.io/mdBook/
* http://docs.godotengine.org/en/latest/index.html

## Additional Assets

* https://opengameart.org/
